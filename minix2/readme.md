# ***Mini-X 2***
### ***What Have You Produced?***

For our second Mini-X we had to create some different emojis, using variable geometry. im not going to lie, i've kind of "cheated" in mine, as im not actually using variable geometry to generate the emojis. i'm using premade image assets that i drew by hand in Adobe Illustrator instead. I just got so fixated on the idea of generating random emojis that i got tunnel-vision and didnt use the geometric functions we were supposed to. *However*, i do think that my final program is pretty cool, as it combines several different assets together, to make over 26880 possible emojis (8 accessories, 20 eyes, 21 mouths and 8 possible heads).

### ***Preview***
![The program in action](/minix2/preview.gif "preview")

[**Click here** to run the program.](https://albinooo.gitlab.io/AestheticProgramming/minix2/)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix2/sketch.js)

### ***What does my code do?***

From my prior programming knowledge, i knew about something called "arrays" where you can store multiple values inside, and then call  them back later.

In my code i use "for" loop functions to generate my arrays automatically, allowing me to add new assets to their respective folders easily, and not requiring me to assign every asset to a variable in the array. It can automatically find the images to use in the array, by using a *"concatenation"* to add strings together to create the filenames. Since all the files are named something like *"mouth 0, mouth 1, mouth 2, etc."* i can just add the index number in the array to the *mouth* prefix, and then it'll automatically have found the correct asset to use.

I then use the Random() function to get a random "body part" from each array, and use those to generate the emoji.

Using a frameRate() function at the end, i can limit how fast the program generates new emojis.

### ***Why?***
I deliberately used non-realistic skin tones & heads for my emojis, as i find myself agreeing with a quote from page 41 in the "executing practices" text:

*"Interestingly, Google did not implement the modifiers on their Android platform and continued to render all humanoid emoji as Barbapapa-style blobs in unrealistic yellow.
A Google spokesperson indicated that this was a deliberate choice: “[Google’s] emoji faces are playful and are all about conveying the emotion you’re feeling. They aren’t designed to look human or reflect human characteristics”."*

However, one of my ideas with creating this emoji randomizer was that it should actually convey unusual and even unrealistic emotions.
The idea stemmed from my own tendency to sometimes use random and nonsensical emojis, and often i just have trouble selecting an emoji *(i have a habit of adding an emoji to every text i send)*. So I thought, if i use emojis so often and sometimes even *"randomly"*, why not just have the emojis *actually be* random? That way i'll always have a funny/weird emoji to add, and maybe it'll even succeed in conveying the emotion i was trying to convey.

One could also be inclined as to compare this to ["Go Rando" by Ben Grosser](https://bengrosser.com/projects/go-rando/), which aims to randomize your reaction emojis on Facebook, making it harder for Facebook to track and analyse you. My program could theoretically be used in the same way. So if you're not interested in having your emoji usage analyzed, you could generate new random emojis every time. This of course makes it a bit harder to actually convey the right emotions, but it'll keep you hard to analyse. You could still use normal/real emojis, like you can in *Go Rando* if you feel like the random emojis arent doing the job.

So, now with this code you can generate new emojis, whether you're trying to convey a weirdly specific emotion, make an entirely new emotion, protect your privacy or if you just want to create something really stupid.

## References & Materials

[Random image from array in p5js:](https://www.youtube.com/watch?v=hxjEl-pun7o)

[Reference p5js project:](https://editor.p5js.org/jarivkin/sketches/84aiAre_5)

["Concat" explanation:](https://www.w3schools.com/jsref/jsref_concat_string.asp)

["For" loop explanation:](https://www.w3schools.com/js/js_loop_for.asp)

[EXECUTING PRACTICES](http://www.data-browser.net/db06.html)

["Go Rando" by Ben Grosser](https://bengrosser.com/projects/go-rando/)
