//creating the variables.
var accessoryArray = [];
var accessoryRandom;
var accessoryNum = 8;

var eyesArray = [];
var eyesRandom;
var eyesNum = 20;

var mouthArray = [];
var mouthRandom;
var mouthNum = 21;

var headArray = [];
var headRandom;
var headNum = 8;

function preload () {
  /*creating arrays containing all the different assets, for each "body part" of the emoji.
  this is pretty complicated, since im using a "for loop" and a concatenation to automatically create the array.

  the "for" loop works like this.
  1. to start with, its sets an initial variable, "i" to the value of zero.
  2. it then defines a "condition" for the loop, in this case it checks if "i" is less than accessoryNum (a variable i set according to the amount of accessories there are to pick from).
  3. it then adds +1 to "i" until the condition is on longer fulfilled, and stops the loop.
  it will keep running the function inside the "{}" once per loop, until step 3 is done.
  */

for (var i = 0; i < accessoryNum; i++) {

  /*
creates an array using the "i" value from the loop, and storing the pictures directly into the array, using a "concatenation" to automatically load the pictures by adding the index numbers to the filename.
a concatenation is when you add two or more text strings together. Here i use it to automatically add "+1" to the end of "accessory", resulting in accessory1, accessory2, accessory3 etc.
this only works because of the way i've named my files. in the beginning i had unique "names" for each asset, like "happy", "sad" and so on.
But by having each name start with the same prefix and then a number, i can automatically grab the file by using "accessory" + a number.
*/
    accessoryArray[i] = loadImage("assets/accessory/accessory" + i + ".png")
  }

//repeating for each "part" of the emoji.
  for (var i = 0; i < eyesNum; i++) {
      eyesArray[i] = loadImage("assets/eyes/eyes" + i + ".png")
  }

  for (var i = 0; i < mouthNum; i++) {
      mouthArray[i] = loadImage("assets/mouth/mouth" + i + ".png")
  }

  for (var i = 0; i < headNum; i++) {
      headArray[i] = loadImage("assets/head/head" + i + ".png")
  }
}

function setup() {
  // put setup code here
  createCanvas(975, 1075);

}

function draw() {
  background(255);

//creates a random head variable by calling the headArray, using the random function to get a random value in the array.
headRandom = random(headArray)
//places it on the site.
  image(headRandom, 50, 50);

eyesRandom = random(eyesArray)
  image(eyesRandom, 50, 50);

mouthRandom = random(mouthArray)
  image(mouthRandom, 50, 50);

accessoryRandom = random(accessoryArray)
  image(accessoryRandom, 50, 50);

//setting the framerate to 1 makes the program regenerate a new emoji every second
frameRate(0.2)

//OR by using a noLoop function, i could stop it from making any new emojis. Which would therefore require a refresh of the page to create a new one.
//noLoop();
}
