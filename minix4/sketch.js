var bgColor = '#c9d0c5'
var nameInput
var addressInput
var x = 1
var y = 1
var easing = 0.05
var buttonX = [1150, 1450, 1550]
var buttonY = [550, 600, 650, 700, 750]
var activeFont
var activeIris

function preload(){
  eyes = loadImage("assets/eyes.png");
  iris = loadImage("assets/iris.png");
  dollarsign = loadImage("assets/dollarsign.png");
  ambience = loadSound("assets/ambience.m4a")
  bold = loadFont("assets/arialBlack.ttf")
  classy = loadFont("assets/timesNewRomanBold.ttf")
  fun = loadFont("assets/comicSansBold.ttf")
}

function setup() {
//starts ambient track on loading
  ambience.play()
    createCanvas(1920, windowHeight);
    background(bgColor)
    activeFont = bold

//name input
  nameInput = createInput("CONSUMER");
  nameInput.size(240)
  nameInput.position(260, 550);

//address input
  addressInput = createInput("LOCATION");
  addressInput.size(300)
  addressInput.position(615, 550);

//personality buttons
buttonBold = createButton("I'M BOLD")
  buttonBold.position(buttonX[0]-29, buttonY[0]);
    buttonBold.mousePressed(function fontBold(){
      activeFont = bold
    })

buttonClassy = createButton("I'M CLASSY")
  buttonClassy.position(buttonX[0]-37, buttonY[2]);
    buttonClassy.mousePressed(function fontClassy(){
      activeFont = classy
    })

buttonFun = createButton("I'M FUN")
  buttonFun.position(buttonX[0]-25, buttonY[4]);
    buttonFun.mousePressed(function fontFun(){
      activeFont = fun
    })

//favorite color buttons
buttonWhite = createButton("WHITE")
  buttonWhite.position(buttonX[1], buttonY[0]);
    buttonWhite.mousePressed(function bgWhite(){
      bgColor = color('#ffffff')
    })

buttonBlack = createButton("BLACK")
  buttonBlack.position(buttonX[2], buttonY[0]);
    buttonBlack.mousePressed(function bgBlack(){
      bgColor = color('#2b2a27')
    })

buttonRed = createButton("RED")
  buttonRed.position(buttonX[1], buttonY[1]);
    buttonRed.mousePressed(function bgRed(){
      bgColor = color('#cf2929')
    })

buttonOrange = createButton("ORANGE")
  buttonOrange.position(buttonX[2], buttonY[1]);
    buttonOrange.mousePressed(function bgOrange(){
      bgColor = color('#cf6629')
    })

buttonYellow = createButton("YELLOW")
  buttonYellow.position(buttonX[1], buttonY[2]);
    buttonYellow.mousePressed(function bgYellow(){
      bgColor = color('#d4bb3d')
    })

buttonGreen = createButton("GREEN")
  buttonGreen.position(buttonX[2], buttonY[2]);
    buttonGreen.mousePressed(function bgGreen(){
      bgColor = color('#6b8a63')
    })

buttonBlue = createButton("BLUE")
  buttonBlue.position(buttonX[1], buttonY[3]);
    buttonBlue.mousePressed(function bgBlue(){
      bgColor = color('#3d6da1')
    })

buttonPurple = createButton("PURPLE")
  buttonPurple.position(buttonX[2], buttonY[3]);
    buttonPurple.mousePressed(function bgPurple(){
      bgColor = color('#664e9c')
    })

buttonPink = createButton("PINK")
  buttonPink.position(buttonX[1], buttonY[4]);
    buttonPink.mousePressed(function bgPink(){
      bgColor = color('#b071ac')
    })

buttonBrown = createButton("BROWN")
  buttonBrown.position(buttonX[2], buttonY[4]);
    buttonBrown.mousePressed(function bgBrown(){
      bgColor = color('#4f341e')
    })

frameRate(60)
}

function draw() {
  clear()
  background(bgColor)

    imageMode(CENTER)

//making an if statement that checks if a key is courrently being held down. if there is, the iris will change to a dollar sign.
if (keyIsPressed === true) {
    activeIris = dollarsign
  } else {
    activeIris = iris
}
/*
Setting up the "easing" of the movement of the eyes.

"Between drawing each frame of the animation, the program calculates the difference between the position of the symbol and the cursor.
If the distance is larger than 1 pixel, the symbol moves part of the distance (0.05) from its current position toward the cursor."
- https://p5js.org/examples/input-easing.html
*/
  var distanceX = mouseX - x;
  x += distanceX * easing;

  var distanceY = mouseY - y;
  y += distanceY * easing;

//Left iris
  var irisLX = constrain(x, 600, 750);
  var irisY = constrain(y, 200, 285);
  image(activeIris, irisLX, irisY);

//Right iris
  var irisRX = constrain(x, 1170, 1320);
  image(activeIris, irisRX, irisY);

//drawing the eyelids
  tint(bgColor)
  image(eyes, width/2, 200)

//static text
  textAlign(CENTER)
  fill(0)
  textFont(activeFont)

textSize(15)
  text("IMPROVE YOUR ONLINE EXPERIENCE.", width/2, 20)
  text("HELP US HELP YOU.", width/2, 40)

textSize(30)
  text("DO YOU FEEL CATERED TOWARDS?", width/2, height-25)

textSize(25)
  text("YOUR NAME", width/5*1, 530)
  text("PLACE OF RESIDENCE", width/5*2, 530)
  text("DESCRIBE YOURSELF", width/5*3, 530)
  text("YOUR FAVORITE COLOR", width/5*4, 530)

textSize(70)
  text("TELL US ABOUT YOURSELF,", width/2, 425)

name = nameInput.value();
address = addressInput.value()

//  var address = "FROM " + addressInput.value()
  text(name + " FROM " + address, width/2, 495)
}
