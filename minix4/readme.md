# ***Mini-X 4 - TELL US ABOUT YOURSELF***

## ***Provide a title for, and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.***
I didnt set out with a title in mind when i started programming, much less a clear idea of what i even wanted to make, but i think an okay title for the finished piece could be ***"TELL US ABOUT YOURSELF"***, obviously in reference to the big piece of text right in the middle of it all. The *"Call for Works"* asked people to make and present:

*"proposals for action and (-de)constructive counter-action including miscalculation and disproportioning, overidentification and obfuscation, metamodeling and re-purposing, acceleration and exaggeration, de-gamification and counter-gamification"*

My program could be seen as a "critical" work that exaggerates how ones ever move online is captured and logged for the express purpose of being sold on to third parties, to then be used the sell products back to you. Its a system where you give your information away for free, to then get "betrayed" by it by getting targeted advertisements. My program takes an approach that lets the user input some info about themselves which then "tailors" the site to them, much like how companies use the captured data to *"make your online experience better"*, or *"help us sell stuff to you"*.

### ***Preview***
![The program in action](/minix4/preview.gif "preview")

[**Click here** to run the program.](https://albinooo.gitlab.io/AestheticProgramming/minix4/) (_it helps the sound load if you click inside the canvas while its loading_)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix4/sketch.js)

### ***Describe your program and what you have used and learnt.***
For our fourth Mini-x the theme was "Data Capture", and we had to try to incorporate various data captuing elements, such as audio, mouse, keyboard, webcam and/or more. The purpose with all this was to critically reflect upon the process of data capture and datafication. In my program i tried some new functions out for the first time, like createButton(), createInput() and mouse tracking. I used mouse tracking to track the eyes to the mouse, i used createButton and createInput to give the user more direct control over what data is collected by the program, as they can write and choose what the program does.

### ***Articulate how your program and thinking address the theme of “capture all.”***
My program takes a pretty *on-the-nose* approach to visualizing data capture. I make the immediately aware that their mouse movement is being tracked, by having a large ominous pair of eyes following their every move across the screen. I have 2 input boxes for the user to fill in info about themselves, with the users name already being "consumer" to furhter highlight the "data capitalism" aspect. If the user chooses to divulge their name and address, the text in the center suddenly becomes targeted not just at the "consumer" but directly at the user. During the program, there is an deep, ominous, droning, ambient track playing in the background, which helps in granting a monotonous and dreadful tone to the page. The reselling of user-data to third parties is made obvious by the eyes iris' turning into dollarsigns whenver a key is pressed, showing how ever click (even a typo), is worth something when it comes to data capturing. Thinking about how data captruing is usually used to direct marketing towards users, and the reasoning they usually give the user is to *"make their expereince on the site better"*, i decided to let the user choose their personality and favorite color, giving a kind of "targeted" advertising feel to the website (further accentuated by the text in the top and bottom about the site *"catering"* to the user and *"improving their online experience"*).

### ***What are the cultural implications of data capture?***
As Shoshana Zuboff touched upon in the Surveillance Capitalism video, the cultural and social implications of mass datafication and logging of digital behaviors are leading to businessed often times knowing people better than they could ever know themselves. The power that they've been able to gain from the relatively un-regulated data-surveillance market has allowed the market to spiral out of control, with users actual consent to being "tracked" long forgotten. Like  the example of the Nest thermostat where you have no idea who gets your data, who those companies then give your data on to, however you *can* opt-out of it after going through something-1000 contracts but with the usability of your thermostat going down the drain, effectively holding the product hostage if you dont willingly hand over the rights to your data.

# References & Materials:

[Eyelids](https://dribbble.com/shots/5737697-A-Slightly-Animated-Eye)

[Ambience](https://www.youtube.com/watch?v=cvLSAqeXJ7s)

[Constrain](https://p5js.org/reference/#/p5/constrain)

[Buttons](https://p5js.org/reference/#/p5/createButton)

[Input](https://p5js.org/reference/#/p5/createInput)

[Change while key is pressed](https://editor.p5js.org/kjhollen/sketches/B7SA0Lr6J)

[Smoothing eye movement with "Easing"](https://p5js.org/examples/input-easing.html)

## Fonts:

[Arial Black](https://www.download-free-fonts.com/details/89466/arial-black)

[Times New Roman Bold](https://www.download-free-fonts.com/details/86877/times-new-roman-bold)

[Comic Sans Bold](https://www.download-free-fonts.com/details/85120/comic-sans-ms-bold)




