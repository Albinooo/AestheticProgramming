# ***Mini-X 6: Fishing Game (Prototype)***
## ***Please run the program before reading :)***

### ***Preview***
![The program in action](/minix6/preview.gif "preview")

[**Click here** to run the program.](https://albinooo.gitlab.io/AestheticProgramming/minix6/)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix6/sketch.js)

## ***how does/do your game/game objects work?***
My program is a fishing "mini-game", where you have to avoid getting caught by the fish (sounds counter-intuitive, i know). All it does is it basically tracks the hook to you current cursor position, and if you overlap with a fish, you lose. Theres also a score system, that counts up how long youve "stayed alive", and if you beat you current highscore, the highscore counter will begin counting along with your current score until you "die". When you die you can press a reset button, that reloads the fishes and resets you current score, but keeps your highscore.

## ***Describe how you program the objects and their related attributes, and the methods in your game.***
as described before, the score system and the reset system is pretty simple. the "difficult" part for me this time was learning how to do classes and objects. The reason for that is you have to do quite a lot of "pre-planning" in comparison to how we have been coding until now. When setting up your classes & objects, you have to do some abstraction, which involves:

*"Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic.1 The main goal is to handle an object’s complexity by abstracting certain details and presenting a concrete model."* [Aesthetic Programming - chapter 6](https://aesthetic-programming.net/pages/6-object-abstraction.html)

so when i was setting up my class for my "fish" (orange rectangles), i had to figure out which components i would be needing when i had to construct them, which is conveniently done in the "constructer" part of the class. But it also allows for a simpler implementation throughtout the rest of the coding, as it lets me create several copies of the same "fish" but still make them different.

## ***Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?***
the game itself could be connected to the fishing industry, and could be seen as a commentary on how its harming the oceanlife all around the world. So instead of a  game where you participate in the fishing industry, you instead try to stay out of it. in relation to "abstraction" one might say that the the stylistic interpretation is an abstraction of the real appearance of fishing, with very abstracted shapes used to represent more complex beings.

# References & Materials:

[Array of objects](https://p5js.org/examples/objects-array-of-objects.html)

[Tofu game example](https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction)

[remove()](https://p5js.org/reference/#/p5.Element/remove)

[Objects](https://p5js.org/examples/objects-objects.html)

[dist()](https://p5js.org/reference/#/p5/dist)
