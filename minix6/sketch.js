var fishSide = [-50, 500+50]
var fish = [];
var minFish = 10
var hookConstrainX
var hookConstrainY
var currentScore = 0
var highScore = 0
var restartButton
var hookSize = {
  w:30,
  h:60
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function setup() {
  createCanvas(500,800)
  frameRate(60)
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function draw() {
background("#3486cf")

//drawing the "hook" & line
hookConstrainX = constrain(mouseX, 0, width)
hookConstrainY = constrain(mouseY, 200, 400)
push()
  strokeWeight(2.5)
  stroke(0)
  line(hookConstrainX, 0, hookConstrainX, hookConstrainY)
pop()
push()
  rectMode(CENTER)
  fill(255)
  noStroke()
  rect(hookConstrainX, hookConstrainY, hookSize.w, hookSize.h)
  fill(255,0,0)
  rect(hookConstrainX, hookConstrainY, hookSize.w, hookSize.h/4)
pop()

//calling the multiple functions to check and generate new fishes
checkFishNum()
showFish()
checkFish()
checkCollision()

//counting up the current score by adding one 60th of a second, every frame. so with a framerate of 60, the user gets one whole point every second.
currentScore += 1/60
fill("gold")
textSize(20)
text("CURRENT SCORE: " + round(currentScore,2),20,30)

//checking if the users current score is over their previous highscore. if it is, the highscore will get replaced by the current score and get saved.
if (highScore <= currentScore){
  highScore = currentScore
}
text("HIGHSCORE: " + round(highScore,2),20,50)
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
class Fish{
  constructor(){
//setting up the "random" width and height the fishes can have
    this.fishW = int(random(75, 150))
    this.fishH = int(random(20, 60))
//Picks a random starting side for the fish, from an array (either left or right)
    this.posX = random(fishSide)
//picks a random starting Y location of the fish, between 2 values
    this.posY = int(random(200,900))
      if (this.posX==fishSide[0]){
        this.speedX=int(random(2, 5))
      } else {
        this.speedX=int(random(-2, -5))
      }
    }
//moving the fish
  move(){
    this.posX+=this.speedX
    this.posY+=-1
  }
//drawing the "fish" which is represented as a rectangle.
  show(){
    rectMode(CENTER)
      fill("#db3725")
        noStroke()
        rect(this.posX, this.posY, this.fishW, this.fishH)
  }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function checkFish() {
//  making a for loop that goes through and checks if a fish has gone off screen, and if it has it will splice a new one to the array.
  for (var i = 0; i < fish.length; i++) {
//checking if the fish has gone off the left side OR the right side OR the top of the screen.
      if (fish[i].posX <= -fish[i].fishW/2 || fish[i].posX >= width+fish[i].fishW/2 ||  fish[i].posY <= 0-fish[i].fishH/2){
//if a fish has gone off screen, splice a new fish into the array.
        fish.splice(i,1);
    }
  }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function checkCollision(){
  for (var i = 0; i < fish.length; i++) {
//  making a variable called "distance" that calculates the distance between the center of the hook to the center of each fish.
    var distance = dist(hookConstrainX, hookConstrainY,  fish[i].posX, fish[i].posY)
//If the hook and the center of the fish overlap, the text "game over" will appear, the noLoop() cuntion will stop the game, and a restart button will appear.
      if (distance <= hookSize.h/2){
        fill(255,0,0)
        textSize(50)
        text("GAME OVER",width/2-160, height/2-10)
        noLoop()
        restartButton = createButton("RESTART")
        restartButton.position(width/2-50,height/2)
//if the player presses the button, the restartGame function is called
        restartButton.mousePressed(restartGame)
    }
  }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function restartGame(){
//removes teh restart button
  restartButton.remove()
//Resets the current score to zero
  currentScore=0
//resets the fish array
  fish=[]
//re-enables looping
  loop()
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function checkFishNum(){
//if the amount of fish is below the minimum number of fish, push a new fish into the array
  if (fish.length < minFish){
    fish.push(new Fish())
  }
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function showFish(){
//goes through the array of fish, and moves each one, and updates their appearance with "show"
  for (var i = 0 ; i<fish.length ; i++){
  fish[i].move()
  fish[i].show()
  }
}
//---------------------------------------------------------------------------------------------------------------------------END------------------------------------------------------------------------------------------------------------------------------------------------
