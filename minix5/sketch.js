var colorArray = [];
var r,g,b
var colorCount
var splatterCount
var palettex

function preload(){
splatter =  loadFont("splatter.ttf")
}

//Making a function that picks a new set of RGB values ehnwever it gets called
function pickColor(){
  r = int(random(30, 170))
  g = int(random(30, 170))
  b = int(random(30, 170))
}

// Making a function that generates a "random" splatter whenever it gets called.
function randomSplatter(){
//selecting the "splatter" font, the font size and disabling the stroke.
  textFont(splatter)
  textSize(int(random(50,300)))
  noStroke()
//filling the splatter with a random color from the colorArray
  fill(random(colorArray))
//getting a random character from a-z, using the "char" function to convert the random integer into a letter that can be read by the text function.
  text(char(int(random(65, 90))), 0, 0)
}

function createColorArray(){
//Clearing the colorArray, so that no colors from previous palettes remain.
colorArray = [];
//setting the colorCount to the current value on the colorCount slider.
colorCount = colorCountSlider.value()
//making a for loop that checks how many colors the user wants via the colorCount, and will repeat the function that amount of times.
    for (var i=0 ; i<colorCount ; i++){
//calling the pickColor funtion to generate a new set of "random" RGB values
      pickColor()
//setting the current index [i] value of the array, to a set of RGB values created by the pickColor function.
      colorArray[i] = [r, g, b]
    }
}

function drawSplatter(){
splatterCount = splatterCountSlider.value()
background(random(colorArray))
  for (var a=0; a<splatterCount; a++){
//using push() & pop() to translate each individual splatter, by placing it somewhere random inside the canvas, and giving it a random rotation.
      push()
        translate(int(random(800)),int(random(800)))
        rotate(int(random(360)))
        randomSplatter()
      pop()
  }
}

function setup(){
  createCanvas(800, 900)
//creating the buttons and sliders to let the user control the artwork.
colorCountSlider = createSlider(2, 20, 2, 1);
  colorCountSlider.position(20,800)

splatterCountSlider = createSlider(0, 1000, 500, 50);
  splatterCountSlider.position(200,800)

createColors = createButton("generate new palette")
  createColors.position(20,840)
    createColors.mousePressed(createColorArray)

generateArtwork = createButton("CREATE ARTWORK")
  generateArtwork.position(200,840)
    generateArtwork.mousePressed(drawSplatter)
//creating an initial colorArray, by calling the createColorArray function.
  createColorArray()
}
function draw() {
//making
fill(255)
rect(0,800,800,100)

//setting the starting x-value of the palette color boxes
palettex = 20
//making a for loop that checks how many colors the user wants via the colorCount, and will repeat the function that amount of times.
  for (i=0 ; i<colorCount ; i++){
  //filling the palette box with the color in the current index position. so if we're in loop 7 of this function, it will take color 7 in the colorArray.
    fill(colorArray[i])
  //placing the first palette color box at "palettex" and then after each loop, it will add 25 to the current x-value, making sure that the next palette box will not overlap the current one.
      rect(palettex,870,20,20)
    palettex += 25
  }
textSize(20)
fill(0)
// creating text that tell the user how many colors & splatters are currently "selected", by gettting the values from the slider and using a concatenation to add the strings together.
text("i want "+colorCountSlider.value()+" colors",20,835)
text("i want "+splatterCountSlider.value()+" splatters",200,835)
}
