# ***Mini-X 5 - Abstract Expressionism Generator***
## ***Please run the program before reading :)***

### ***Preview***
![The program in action](/minix5/preview.gif "preview")

[**Click here** to run the program.](https://albinooo.gitlab.io/AestheticProgramming/minix5/)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix5/sketch.js)

## ***What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?***
The *"rules"* set in my program, are the amount of splatters, their size, their possible colors and where they're placed. The tasks set for the RunMe did state that the program should be *"without any direct interactivity"*, but i still thought it would be cool to let the user atleast have *some* control of the program, so i decided to let teh user control how many different colros the program can choose from, and how mayn splatters it should draw. The user still doesnt have any control over the splatters location, rotation, size or individual color, although they can keep generating a new palette until they have something theyre happy with.

### ***Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?”***
While reading the part about *"Computational Randomness in the Arts" in the *10 PRINT* text, i noticed the programs dont leave a lot of interactivity to the user, and they instead opt for a much more program-oriented generative experience. In contrast to those examples, my program has a lot of interactivity, but almost none if compared to any "real" program. So, the levels of control can vary heavily in programming, depending on how much autonomy the developer wants the user to have.
The subject of autonomy is touched furhter upon in the *"Ten Questions Concerning Generative Computer Art"* text:

*"The degree of autonomy and independence assigned to the computer varies significantly - from works that seek to minimize or exclude the creative "signature" of the human designer to those in which the computer's role is more passive and the human artist has primary creative responsibility and autonomy."*

So, from this we can safely say that my program falls more into the latter category, since i hand *some* control over to the user. However the program is still mostly generative, as it still adheres to he same ground rules every time it runs.

# References & Materials:

[char()](https://p5js.org/reference/#/p5/char)

[Splatter Font](https://www.dafont.com/splatter.font)
