# ***Mini-X 3***
### ***What Have You Produced?***

For the third Mini-X we had to create a throbber, preferably using some of the new syntax we learned which included *for loops* and *arrays*. I didnt really use the "*for loops*" to their full extent this time, like i did in my previous minix. i also didnt use any arrays this time either. I had a really hard time coming up with an idea for this minix, so much so that i didnt *technically* come up with the idea i ended up coding. This ***"Newton's cradle"*** that i've made was actually mostly Maikens idea originally *(except for the color changing and the sound)*, and when i was just trying to help her create it, i "accidentally" just programmed it and she ended up doing something different anyways. So, big thanks to Maiken for letting me have this idea **:^)**.

The actual program itself is just an infinitely looping ***Newton's Cradle***, that changes color whenever it hits/bounces, and i also used the p5js sound library to add a really stupid and out of place crashing sound whenever it changes color.

### ***Preview***
![The program in action](/minix3/preview.gif "preview")

[**Click here** to run the program. **(Click inside the page once it loads, otherwise sound wont play. Btw it might be loud, idk)**](https://albinooo.gitlab.io/AestheticProgramming/minix3/)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix3/sketch.js)

### ***What does my code do?***

This program can largely be compared to my first minix, where i made the old DVD standby screen, where the DVD logo bounces around the edges of the screen and changes color. We can see a lot of that in this same program, and i even went back and looked at my old code again a couple of times to see how i did some of the things the first time. For example, the *"pickcolor"* function that i use here is basically lifted straight from that first minix, and i just reused it again here.

most of the code is pretty "boring", just me drawing the shapes and rotating them around. the only *interesting* part is down at the bottom, where i use a weird chain of "*else if*" statements to make the cradle detect when its getting hit, what side it should change to, and when it should change directions.
The problem i ran into when coding this part, was detecting which direction the ball was coming from. Since there are 2 points in time where all the circles will be touching, its important to know which circle is coming back down and hitting the other ones, so i know which side should begin its swing afterwards. i did this by making a *stage* variable, that keeps track of where the ball is coming from.

## References & Materials

[crash sound effect](https://freesound.org/people/qubodup/sounds/151624/)

[My first MiniX](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix1/readme.md)
