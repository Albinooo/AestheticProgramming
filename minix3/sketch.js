// (0, -250) measured form the center of the circles
var pivot_x = 0;
var pivot_y = 0;

// Position of the left pivot point
var target_x1 = 250;
var target_y1 = 00;

var angleL = 0;
var angle_changeL = 1;

// Position of the left pivot point
var target_x2 = 650;
var target_y2 = 00;

var angleR = 0;
var angle_changeR = 0;

//the spacing between the circles
var circleSpacing;

// the "stage" which the pendulum is in. very important variable, since it helps the program know if the ball is etiehr supposed to be going away or if it is returning.
var stage= 0;

var r,g,b
var pickColor

//preloading the sound before anything else
function preload(){
  crash = loadSound("crash.mp3");
}


function pickColor(){
r = random(100, 255)
g = random(100, 255)
b = random(100, 255)
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    pickColor()

}

function draw() {
    background(250)
    angleMode(DEGREES)

//apparently saying "+=" is the same as saying: angleL = angleL + angle_changeL
angleL+=angle_changeL
angleR+=angle_changeR

//drawing the left string
  push()
//honestly i have no idea why i need the translate function here, but it doesnt work without it.
    translate(250, 0);
    // for some reason i also need to add 2 rotate functions, otherwise the line wil rotate around the bottom of itself, so i just rotate it 180 degrees to make it rotate the right way up
    rotate(180)
    rotate(angleL)
    stroke (0)
    line(0, -250, 0, 0);
  pop()

// drawing the left circle
  push()
    translate(target_x1, target_y1);
    rotate(angleL);
    translate(-pivot_x, -pivot_y);

      noFill();
      stroke(0)
      ellipseMode(CENTER)
      noStroke()
      fill(r,g,b)
      ellipse(0,250,100,100);
  pop();

//drawing the left string
  push()
    translate(650, 0);
    rotate(180)
    rotate(angleR)
    stroke (0)
    line(0, -250, 0, 0);
  pop()

//drawing  the right circle
  push()
    translate(target_x2, target_y2);
    rotate(angleR);
    translate(-pivot_x, -pivot_y);

      noFill();
      stroke(0)
      ellipseMode(CENTER)
      noStroke()
      fill(r,g,b)
      ellipse(0,250,100,100);
  pop();

// checks if we are in the begininng stage (0), and if the angle of the left circle is 0 (it is at the bottom of its swing)
// instead of making this really convoluted "else if" system, i couldve used a swtich case. i just didnt.

//here i check if the angle of the angle of the left circle is 0, which means its at the bottom if its swing. i also check if its in "stage 0", which means the cradles cycle is at the very beginning.
//If both of these conditions are met, then its sets the angle_changeL to move with 1 degree every frame
    if (angleL == 0 && stage == 0) {
      angle_changeL = 1

//here i check if the left circle has reached an angle of 50 degrees, and if it has, hten i reverse the angle change, making it go "-1" degree every frame. then i also increase "stage" to 1, so the program can recognize that the ball is now coming back down again.
      } else if (angleL == 50){
        angle_changeL = -angle_changeL;
        stage = 1

//now i check if the left circle has reached 0 degrees again, and if its in stage 1 (coming back from its swing), then i set the angle_changeL to 0, meaning the left ball will stop moving.
//then it changes the angle_changeR to -1, meaning it will begin its swing (its negative because its going to the right now).
//And lastly i make all the circles change color, and play the crash sound effect.
        } else if (angleL == 0 && stage==1) {
          angle_changeL = 0;
          angle_changeR = -1;
          stage = 2;
          pickColor();
          crash.play();

//this is the same as before, where i just check if its reached -50 degrees and then invert the angle change.
            } else if (angleR == -50) {
              angle_changeR = -angle_changeR

//this is pretty much the same as before yet again, except after stopping the right ball, i dont start the angle change of the left one. instead i go back to stage 0, meaning the first "if" statement will start again, and this makes the program loop.
            } else if (angleR == 0 && stage == 2){
                angle_changeR = 0
                stage = 0;
                pickColor();
                crash.play();
    }

//loop for creating the 3 middle circles + lines
circleSpacing = 350
  for (var i = 0; i<3 ; i++){
    stroke (0)
    line(circleSpacing, 0, circleSpacing, 250);
    noStroke()
    fill(r, g, b)
    ellipse(circleSpacing,250,100,100)
circleSpacing += 100
    }
  frameRate(60)
}

//  https://stackoverflow.com/questions/65900807/how-do-i-rotate-a-rectangle-shape-from-a-specific-point-in-p5-js
