# ***Mini-X 7: Fishing Game (version 2)***
## ***Please run the program before reading :)***

### ***Preview***
![The program in action](/minix7/preview.gif "preview")

[**Click here** to run the program.](https://albinooo.gitlab.io/AestheticProgramming/minix7/)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix7/sketch.js)

## ***Which MiniX have you reworked?***
For my Mini-X 7 i chose to rework my last Mini-X, my Mini-X 6. Last time i made a pretty simeple fishing game, which

## ***What have you changed and why?***
the only thiing i really changed in this version of my Mini-X, is i added a lot of assets to imporve the appearance of the overall game, and i also fixed teh hitboxes of the fish so that instead of detecting if you hit the middle of the fish, you now have to hit the mouth of the fish to die (which seemed a bit more "realistic"?). I also made the hook control smoother, having it have a slight delay after the cursor, which increased teh difficulty a bit. but other than those things i didnt really change a lot. 

## ***What have you learnt in this mini X? How did you incorporate/advance/interprete the concepts in your ReadMe/RunMe (the relation to the assigned readings)?***
i improved my knowledge and skills with classes & objects a bit, since i added a feature that now randomizes the color of each individual fish. This was kinda cool, since i used previous knowledge and code from my other mini-x's to improve upon this one. The random color function is something i've used in almost every mini-x, and i also re-used the smoothed mouse movement from my mini-x 4 this time.

## ***What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?***
to use a quote from the preface of the aesthetical programming book:

*"To reiterate the point, our intention is for readers to acquire key programming skills in order to read, write and think with, and through, code (and we will return to the issue of literacy later). We feel that it is important to further explore the intersections of technical and conceptual aspects of code in order to reflect deeply on the pervasiveness of computational culture and its social and political effects — from the language of human-machine languages to abstraction of objects, datafication and recent developments in automated machine intelligence, for example."*

In modern times, digital culture is of course always ever changing, and the pace at which it is evolving increases along with it. So aesthetic programmings focus on giving the user  key programming skills like reading and writing atleast a basic level of code, helps a lot in letting the person know how wand what is actually going on "behind the scenes" so to speak. and when a user knows how to grasp this srot of stuff, it will let them think deeper about the societal/political implications that they themselves have on the digital culture they find themselves in, and how to shape it.

# References & Materials:

[Array of objects](https://p5js.org/examples/objects-array-of-objects.html)

