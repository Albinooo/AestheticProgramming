# ***Mini-X 1***
### ***What Have You Produced?***

For our first Mini-X we had a lot of freedom to do whatever we wanted, so i felt like doing something "basic" but still somewhat impressive. I wanted to do something that included movement, since a static image  wouldnt be very exciting to look at in my opinion. For inspiration i decided to look at various random programming videos, and in one of them i saw something bounce off the side of the screen (cant remember what the actual video was), and it reminded me of that old DVD standby screen, with the "DVD Video" logo bouncing around and changing colors when it bounced off teh side of the scrren, leaving you waiting for when it would finally hit the corner. I decided i wanted to replicate that, as it seemed like a good, simple project that would require me to learn some useful new functions. In the original DVD-player version there isnt any sort of sound, its just the logo floating around in a silent, black, empty void. However, i decided that i wanted to add a "boop" sound effect whenever the image hits the sides of the screen, just for teh challenge of it, and because it would give more feedback to the user. The program doesnt allow for any user interaction while its running, its just a sort of *"nostalgic screensaver"* if you will.

### ***My Independent Coding Experience***
In the beginning, i had the idea that i wanted to try and manually create/write the entire code by hand without any external help, and only by finding the relevant functions in the P5JS reference list. I soon found out however, that it would take me forever to find the functions i should use, and how to use them correctly. So i decided to google "how to make image bounce in JavaScript" and surprisingly a video came up detailing how to do exactly the DVD Logo bounce, and the tutorial was even in P5JS, which was really lucky (i'll leave a link to the video down at the bottom).

So my first "independent" coding experience wasnt *all* that independent, but reading and altering code other peoples code is one the best ways to learn how to be a more effective programmer yourself. I didnt want to just "steal" his code though, so i tried using my (limited) prior coding knowledge to make it my own, by using variables to make the code more easily customizable in various places (just in case i should ever want to). I also wanted to try my hand at the P5JS sound library, so i added a "boop" sound every time the logo hits one of the sides of the canvas. The sound part i actually did figure out myself by looking at the P5JS sound reference list, and figured out how to implement it.

### ***Preview***
![The program in action](/minix1/preview.gif "preview")

[**Click here** to run the program.](https://albinooo.gitlab.io/AestheticProgramming/minix1/) (_i found that it helps the sound load if you click inside the canvas_)

[**Click here** to view the code.](https://gitlab.com/Albinooo/AestheticProgramming/-/blob/main/minix1/sketch.js)


### How is the coding process different from, or similar to, reading and writing text?
Writing code is different from normally writing text, in that you are giving very precise instructions to a computer, instead of trying to convey an idea to a person. When you normally write text for humans to understand, you can get away with being vague, like *"when you do this its kiiinda like this, or kiiiinda like that"*, but with a computer you have to be so incredibly specific, or else it wont understand you. While wrinting the code for this Mini-x, i tried to leave as many *"//comments"* as possible, both so it would be easier for the potential reader to figure out how the code and all its parts work, and also so it'll be easier for me to come back and re-read the code in the future if i ever forget how to do something i did here.

Reading code is not *too* different from normal reading, but again you arent trying to get an *idea* that someone wrote, you're trying to understand the instructions they gave the computer. And by figuring out how they did what they did, and what the commands do, you gain a better understanding of programming and how to accomplish those certain features in the future. Like i did with this program, i really had no idea where to start with the code, but watching the tutorial about it and having the different functions and commands explained while using them, i gained a better understanding of those features adn commands, and now know how they function. It's also a great programming skill to know how to find small code snippets online, and knowing how to manipulate and customize the code into your own.

### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
Programming isnt as foreign of a  subject to me as it might be to some other people in this course, but thats not to say im a professional by any means. I've only had some very introductory courses in some languages, so i can't really do a lot with that. The Vee text we read focused a lot on the importance of learning programming, and i partially agree with it. i dont necessarily think *everyone* has to learn to code, but it is indeed a useful skill to have. A miner or a truck driver for example might not have much immediate use for it, but for people wishing for a more "future-proof" job, or something connected with computers, it is indeed a very useful and fulfilling skill to have. For me, who's studying Digital Design, it seems almost self explanatory why someone like me should learn atleast the basics of programming. It's a good thing to know whats achievable digitally when designing digital solutions to problems, and learning to code helps the designer better understand that.

## References & Materials
[Coding Challenge #131: Bouncing DVD Logo](https://www.youtube.com/watch?v=0j86zuqqTlQ)

[DVD Video Logo](https://www.deviantart.com/gbmpersonal/art/DVD-White-logo-355354117)

[Bounce sound effect](https://www.youtube.com/watch?v=w3MevX_jRQw)


