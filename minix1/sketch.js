  //creating the variables im going to use in the rest of the program
var x;
var y;
var canvaswidth;
var canvasheight;
var xspeed;
var yspeed;
var imagex;
var imagey;
var r,g,b;

  //lets me change the dimensions of the dvd logo, without having to manually replace the values of the image everywhere
imagex = 120;
imagey = 80;

  //lets me set the width and height of the canvas, so i can easily change it if i want to.
canvaswidth = 1000;
canvasheight = 600;

  //preloads the assets for later usage
function preload(){
  dvd = loadImage("dvd.png");
  bounce = loadSound("boop.mp3");
}

//picks a random RGB color, with a lower limit of 100, to make sure the colors stay somewhat bright
function pickColor(){
r = random(100, 255)
g = random(100, 255)
b = random(100, 255)
}

function setup() {
  createCanvas(canvaswidth, canvasheight);
  //starting point of dvd logo. it will automatically calculate the center of the canvas by dividing the width and height of the canvas by 2.
  //however that will only place the origin-point (top left corner) of the logo at the center, and by subtracting half of the images width and height, it will bring the center of the image to the center of the canvas.
  x=canvaswidth/2-imagex/2;
  y=canvasheight/2-imagey/2;
  //x and y speed of moving image
  xspeed=2;
  yspeed=2;
  //picks a starting color for the image
  pickColor();
}

function draw() {
  background(0, 0, 0);
    //tint the dvd logo
  tint(r,g,b);
    //use the dvd logo
  image (dvd, x, y, imagex, imagey);
    //this is what actually moves the image. it constantly takes the current coordinate/location of the image and adds the value of the x- and y-speed.
  x = x + xspeed;
  y = y + yspeed;

    //width = width of canvas
    //since the starting point of the image is its upper left corner, it means that it only checks when that corner hits the edges of the canvas.
    //so if the x position of the image is equal to the width of the canvas, it must mean the upper left corner has hit the sides of the canvas.
    //by adding the width of the image to it, we get it to check the right corner too.
  if (x == 0 || x + imagex == width) {
    //reverses the direction by adding a minus in front of the value, so instead of moving +2 on the x axis, it moves -2.
    xspeed = -xspeed;
    //Picks a new color when it reverses direction.
    pickColor();
    //plays the "boop" sound.
    bounce.play();
  }
    //the same as before, but for the y position, and now the height instead of the width.
  if (y == 0 || y + imagey == height) {
    yspeed = -yspeed;
    pickColor();
    bounce.play();
  }
}
